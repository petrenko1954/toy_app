#Листинг 2.10: Ограничение длины микросообщений максимумом в 140 знаков. #app/models/micropost.rb 

class Micropost < ActiveRecord::Base
belongs_to :user

validates :content, length: { maximum: 140 },
                      presence: true

end
